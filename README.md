# Fast RANLUX

This repository contains a C++11 header-only implementation of fast RANLUX pseudo-random number generators well suited for 8-, 16-, 32- and 64-bit processors.

The code snippet below demonstrates how to use the generators (here, a 32-bit generators is used). More information can be found at the beginning of `fast-ranlux.hpp` or in Doxygen documentation generated from it. The code snippet must be compiled as C++11 (or newer) code, e.g., `c++ -std=c++11 example.cpp`.

```c++
#include <cstdio>
#include "fast-ranlux.hpp"
#include <random>

int main()
{
	auto seed = 123456u;
	auto gen = fast_ranlux::ranlux32(seed);
	auto int_dist = std::uniform_int_distribution<int>();
	auto float_dist = std::uniform_real_distribution<float>();
	auto i = int_dist(gen);
	auto f = float_dist(gen);

	std::printf("%d %8.2e\n", i, f);
}
```
