// compile with
// c++ -std=c++11 example.cpp
#include <cstdio>
#include "fast-ranlux.hpp"
#include <random>

int main()
{
	auto seed = 123456u;
	auto gen = fast_ranlux::ranlux32(seed);
	auto int_dist = std::uniform_int_distribution<int>();
	auto float_dist = std::uniform_real_distribution<float>();
	auto i = int_dist(gen);
	auto f = float_dist(gen);

	std::printf("%d %8.2e\n", i, f);
}
