// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef FAST_RANLUX_RANDOM_NUMBER_ENGINE_HPP
#define FAST_RANLUX_RANDOM_NUMBER_ENGINE_HPP

/**
 * @file native-ranlux.hpp
 *
 * This file contains implementations of Method I, II, and III add-with-carry
 * (AWC) and subtract-with-borrow (SWB) pseudo-random number generators as well
 * fast RANLUX generators for 8-, 16-, 32-, and 64-bit machines, cf.
 * * G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The Annals of Applied Probability, Vol. 1, No. 3, pp. 462--480. 1991.
 * * M. Lüscher: "A Portable High-Quality Random Number Generator Lattice Field
 * Theory Simulations". In: Computer Physics Communications, Vol. 79, No. 1,
 * pp. 100--110. 1994. arXiv:hep-lat/9309020v1.
 * * F. James: "RANLUX: A Fortran implementation of the high-quality
 * pseudorandom number generator by Lüscher". In: Computer Physics
 * Communications, Vol. 79, No. 1, pp. 111--114. 1994.
 *
 * The code requires at least C++11.
 *
 * Most users should be able to use one of the predefined RANLUX generators from
 * the \ref generator-list "table of generators" below because their period is much longer than
 * 2<sup>100</sup> and the parameters were optimized for speed. Each generator
 * comes in two flavors: one flavor generates provably uniformly distributed
 * b-bit integers and passes all known empirical PRNG tests; the other variant
 * is prefixed with `fast_`, also returns b-bit integers, also passes all known
 * empirical PRNG tests but is not *proven* to return uniformly distributed
 * integers. The default flavors correspond to luxury level 4 and the fast
 * flavors correspond approximately to luxury level 2.
 *
 * > Use the default flavor if you do not know which generator flavor is best for you.
 *
 * The generators can be used like any other C++ standard library random number
 * engine:
 *
 *     #include <cstdio>
 *     #include "fast-ranlux.hpp"
 *     #include <random>
 *     
 *     
 *     int main()
 *     {
 *     	auto seed = 123456u;
 *     	auto gen = fast_ranlux::ranlux32(seed);
 *     	auto int_dist = std::uniform_int_distribution<int>();
 *     	auto float_dist = std::uniform_real_distribution<float>();
 *     	auto i = int_dist(gen);
 *     	auto f = float_dist(gen);
 *     
 *     	std::printf("%d %8.2e\n", i, f);
 *     }
 *
 * Compile this code with
 *
 *     c++ -std=c++11 example.cpp
 *
 *
 * <table>
 * <caption id="generator-list">Table of generators</caption>
 * <tr><td><b>Random bits per draw</b> <td><b>Default flavor</b> <td><b>Fast flavor</b>
 * <tr><td>8 <td>ranlux8 <td>fast_ranlux8
 * <tr><td>16 <td>ranlux16 <td>fast_ranlux16
 * <tr><td>32 <td>ranlux32 <td>fast_ranlux32
 * <tr><td>64 <td>ranlux64 <td>fast_ranlux64
 * </table>
 */

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <cstring>
#include <type_traits>

#if defined(__SIZEOF_INT128__) && __SIZEOF_INT128__ == 16
#define FAST_RANLUX_HAS_INT128 1
#else
#define FAST_RANLUX_HAS_INT128 0
#endif


#define FAST_RANLUX_VERSION_MAJOR 1
#define FAST_RANLUX_VERSION_MINOR 0
#define FAST_RANLUX_VERSION_PATCH 0
#define FAST_RANLUX_VERSION "1.0.0"


namespace fast_ranlux {
namespace impl_random_number_engine
{
	constexpr std::uint32_t rotl(std::uint32_t x, unsigned k)
	{
		return (x << k) | (x >> (32 - k));
	}


	/**
	 * This class helps to minimize the amount of code needed to switch between
	 * the AWC/SWB implementations requiring 128-bit integers and those that do
	 * not by providing a dummy integer to be used whenever no 128-bit is
	 * available.
	 *
	 * This type allows us to use a simple if statement to switch the
	 * implementation and the optimizer should take care of the rest.
	 */
	struct dummy_integer_t
	{
		dummy_integer_t(std::uintmax_t) {}

		operator std::uintmax_t() const { return 0; }
	};

	dummy_integer_t operator+ (dummy_integer_t, dummy_integer_t) { return 0; }
	dummy_integer_t operator+ (dummy_integer_t, std::uintmax_t) { return 0; }
	dummy_integer_t operator- (dummy_integer_t, dummy_integer_t) { return 0; }
	dummy_integer_t operator- (dummy_integer_t, std::uintmax_t) { return 0; }
	dummy_integer_t operator>> (dummy_integer_t, std::size_t) { return 0;}


#if FAST_RANLUX_HAS_INT128
#if __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
	template<std::size_t w>
	using big_integer_t = typename std::conditional<
		w == 8u,  std::uint16_t, typename std::conditional<
		w == 16u, std::uint32_t, typename std::conditional<
		w == 32u, std::uint64_t, typename std::conditional<
		w == 64u, unsigned __int128, dummy_integer_t
	>::type>::type>::type>::type;
#if __GNUC__
#pragma GCC diagnostic pop
#endif
#else
	template<std::size_t w>
	using big_integer_t = typename std::conditional<
		w == 8u,  std::uint16_t, typename std::conditional<
		w == 16u, std::uint32_t, typename std::conditional<
		w == 32u, std::uint64_t, dummy_integer_t
	>::type>::type>::type;
#endif

}

/**
 * This class implements a xoshiro128+ pseudo-random number generator.
 *
 * D. Blackman, S. Vigna: "Scrambled Linear Pseudorandom Number Generators." 2018. arXiv:1805.01407v1
 *
 * See Table 4 of the paper for TestU01 BigCrush results (column "S" shows
 * "systematic" failures and column "R" contains "repeated" failures"). The
 * public domain code for xoshiro128plus by Blackman and Vigna can be downloaded
 * from the xoshiro/xoroshiro website: http://xoshiro.di.unimi.it
 */
struct xoshiro128plus
{
	using result_type = std::uint32_t;

	static constexpr result_type max() {
		return std::numeric_limits<result_type>::max();
	}
	static constexpr result_type min() {
		return std::numeric_limits<result_type>::min();
	}


	explicit xoshiro128plus(std::uint64_t seed=0u) :
		// initialize state with some ones here to avoid LFSR zeroland
		state_{
			0, ~std::uint32_t{0}, std::uint32_t(seed>>32), std::uint32_t(seed)
		}
	{
		// escape from zeroland, cf. Section 9 in Blackman/Vigna (2018)
		// this generator is fast so generously discard a few values
		discard(128);
	}


	std::uint32_t operator() ()
	{
		using impl_random_number_engine::rotl;

		auto retval = state_[0] + state_[3];
		auto t = state_[1] << 9;

		state_[2] ^= state_[0];
		state_[3] ^= state_[1];
		state_[1] ^= state_[2];
		state_[0] ^= state_[3];
		state_[2] ^= t;
		state_[3] = rotl(state_[3], 11);

		return retval;
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::uint32_t state_[4];
};



/**
 * This class implements an add-with-carry (AWC) pseudo-random number generator.
 *
 * This generator is initialized with the aid of a xoshiro128+ PRNG to avoid
 * correlated outputs for AWC PRNGs with similar seeds.
 *
 * References:
 * * G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The
 *   Annals of Applied Probability, Vol. 1, No. 3, pp. 462--480. 1991.
 * * M. Matsumoto et al.: "Defects in Initialization of Pseudorandom Number
 *   Generators." In: ACM Transactions on Modeling and Computer Simulation,
 *   Vol. 17, No. 4, Article 15. 2007. DOI: 10.1145/1276927.1276928
 */
template<typename T, std::size_t w, std::size_t s, std::size_t r>
struct add_with_carry_engine
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");
	static_assert(w <= std::numeric_limits<T>::digits, "");
	static_assert(s > 0u, "");
	static_assert(r > s, "");

	// prototype implementation
	static_assert(w == std::numeric_limits<T>::digits, "");
	static_assert(w == 8u or w == 16u or w == 32u or w == 64u, "");

	using result_type = T;

	static constexpr auto long_lag = r;
	static constexpr auto short_lag = s;
	static constexpr auto word_size = w;
	// `std::subtract_with_carry_engine::default_seed` is 19780503. I have no
	// idea where this value is coming from.
	static constexpr auto default_seed = std::uint32_t{19780503};


	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }


	explicit add_with_carry_engine(std::uint64_t seed=default_seed)
	{
		auto gen = xoshiro128plus(seed);

		for(auto& x : xs_)
			x = gen();

		carry_ = xs_[0] == 0 ? 1u : 0u;

		// ensure entering a periodic sequence
		discard(r);
	}


	T operator() ()
	{
		using BigInt = impl_random_number_engine::big_integer_t<w>;
		using DummyInt = impl_random_number_engine::dummy_integer_t;

		auto i = index_;
		auto j = index_ >= s ? index_ - s : index_ + r - s;

		assert(carry_ == 0 or carry_ == 1);

		if(std::is_same<BigInt, DummyInt>::value)
		{
			auto x = xs_[i];
			auto y = T(x + xs_[j]);
			auto z = T(y + carry_);

			xs_[i] = z;
			carry_ = y < x or z < y ? 1u : 0u;
		}
		else
		{
			auto x = BigInt{xs_[i]} + xs_[j] + carry_;

			xs_[i] = x;
			carry_ = x >> w;
		}

		index_ = long_lag == 8 or long_lag == 16 or long_lag == 32
			? (index_ + 1u) & (long_lag-1u)
			: index_ + 1u == long_lag ? 0u : index_ + 1u
		;

		return xs_[i];
	}



	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::size_t index_ = 0;
	T carry_ = 0;
	T xs_[r] = { 0 };
};



/**
 * This class implements a subtract-with-borrow (SWB) pseudo-random number
 * generator.
 *
 * This generator is initialized with the aid of a xoshiro128+ PRNG to avoid
 * correlated outputs with similar seeds. In comparison to
 * `std::subtract_with_carry_engine`, this class also supports generators with
 * `p > q`; generators with `p > q` are described as "Type 2" or "Method 2" in
 * the paper by Marsaglia and Zaman.
 *
 * References:
 * * G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The
 *   Annals of Applied Probability, Vol. 1, No. 3, pp. 462--480. 1991.
 * * M. Matsumoto et al.: "Defects in Initialization of Pseudorandom Number
 *   Generators." In: ACM Transactions on Modeling and Computer Simulation,
 *   Vol. 17, No. 4, Article 15. 2007. DOI: 10.1145/1276927.1276928
 */
template<typename T, std::size_t w, std::size_t p, std::size_t q>
struct subtract_with_borrow_engine
{
	static constexpr auto word_size = w;
	static constexpr auto long_lag = p > q ? p : q;
	static constexpr auto short_lag = p > q ? q : p;

	static constexpr auto r = long_lag;
	static constexpr auto s = short_lag;

	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");
	static_assert(w <= std::numeric_limits<T>::digits, "");
	static_assert(short_lag > 0u, "");
	static_assert(long_lag > short_lag, "");

	// prototype implementation
	static_assert(w == std::numeric_limits<T>::digits, "");
	static_assert(w == 8u or w == 16u or w == 32u or w == 64u, "");

	using result_type = T;

	// `std::subtract_with_carry_engine::default_seed` is 19780503. I have no
	// idea where this value is coming from.
	static constexpr auto default_seed = std::uint32_t{19780503};


	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }


	explicit subtract_with_borrow_engine(std::uint64_t seed=default_seed)
	{
		auto gen = xoshiro128plus(seed);

		for(auto& x : xs_)
			x = gen();

		carry_ = xs_[0] == 0 ? 1u : 0u;

		// ensure entering a periodic sequence
		discard(r);
	}


	T operator() ()
	{
		using BigInt = impl_random_number_engine::big_integer_t<w>;
		using DummyInt = impl_random_number_engine::dummy_integer_t;

		auto i = index_;
		auto j = index_ >= s ? index_ - s : index_ + r - s;

		assert(carry_ == 0 or carry_ == 1);

		if(std::is_same<BigInt, DummyInt>::value)
		{
			auto x = p > q ? xs_[i] : xs_[j];
			auto y = p > q ? T(x - xs_[j]) : T(x - xs_[i]);
			auto z = T(y - carry_);

			xs_[i] = z;
			carry_ = y > x or z > y ? 1u : 0u;
		}
		else
		{
			auto x = p > q
				? BigInt{xs_[i]} - xs_[j] - carry_
				: BigInt{xs_[j]} - xs_[i] - carry_
			;
			auto c = -T(x >> w);

			xs_[i] = x;
			carry_ = c;
		}

		index_ = long_lag == 8 or long_lag == 16 or long_lag == 32
			? (index_ + 1u) & (long_lag-1u)
			: index_ + 1u == long_lag ? 0u : index_ + 1u
		;

		return xs_[i];
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::size_t index_ = 0;
	T carry_ = 0;
	T xs_[long_lag] = { 0 };
};



using ranlux8_base = subtract_with_borrow_engine<std::uint8_t, 8, 3, 26>;
using ranlux8 =  std::discard_block_engine<ranlux8_base, 131, 26>;
using fast_ranlux8  = std::discard_block_engine<ranlux8_base, 37, 26>;

using ranlux16_base = subtract_with_borrow_engine<std::uint16_t, 16, 5, 24>;
using ranlux16 = std::discard_block_engine<ranlux16_base, 241, 24>;
using fast_ranlux16 = std::discard_block_engine<ranlux16_base, 61, 24>;

using ranlux32_base = add_with_carry_engine<std::uint32_t, 32, 3, 16>;
using ranlux32 = std::discard_block_engine<ranlux32_base, 277, 16>;
using fast_ranlux32 = std::discard_block_engine<ranlux32_base, 71, 16>;

using ranlux64_base = subtract_with_borrow_engine<std::uint64_t, 64, 62, 3>;
using ranlux64 = std::discard_block_engine<ranlux64_base, 1303, 62>;
using fast_ranlux64 = std::discard_block_engine<ranlux64_base, 331, 62>;

}

#undef FAST_RANLUX_HAS_INT128

#endif
